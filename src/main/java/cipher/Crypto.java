package cipher;

/**
 * Class to encode or decode text messages using the caesar cipher tecnique.
 * @author fionn
 * @version 1.0
 */

public class Crypto {

    private  final int ABC_BASE = 26;

    /**
     * This method encrypts a message
     * @param msg the message to be encrypted
     * @param key the number of shifts a letter must be move. Number is in range of 1-25. Otherwise the encryption doesn't work.
     * @return the encrypted message.
     */
    public String encode(String msg, int key) {
        String encryptedMsg = "";

        for (int i = 0; i < msg.length(); i++) {
            char tmp = msg.charAt(i);
            if (!Character.isLetter(tmp)) {
                encryptedMsg = encryptedMsg + tmp;
            }
            else if (Character.isUpperCase(tmp) && tmp+key > 'Z' || Character.isLowerCase(tmp) && tmp+key > 'z') {
                encryptedMsg = encryptedMsg + (char)((tmp-ABC_BASE)+key);
            }
            else {
                encryptedMsg = encryptedMsg + (char)(tmp+key);
            }
        }
        return encryptedMsg;
    }

    /**
     * This method decrypts a message
     * @param msg the message to bo decrypted
     * @param key must be the same number used to encrypt the message.
     * @return the original message.
     */
    public String decode(String msg, int key) {
        String decryptedMsg = "";

        for (int i = 0; i < msg.length(); i++) {
            char tmp = msg.charAt(i);
            if (!Character.isLetter(tmp)) {
                decryptedMsg = decryptedMsg + msg.charAt(i);
            }
            else if (Character.isUpperCase(tmp) && tmp-key < 'A' || Character.isLowerCase(tmp) && tmp-key < 'a') {
                decryptedMsg = decryptedMsg + (char)((tmp-key)+ABC_BASE);
            }
            else {
                decryptedMsg = decryptedMsg + (char)(tmp-key);
            }
        }
        return decryptedMsg;
    }
}
