package cipher;

public class Application {

    public static void main(String[] args) {

        int key;
        String mode = args[0];
        String message = args[1];
        Crypto crypto = new Crypto();

        switch(mode.toLowerCase()) {
            case "encode" :
                Genkey genkey = new Genkey();
                key = genkey.caesarCipher();
                String encodedMsg = crypto.encode(message, key);
                System.out.println("Encoded message --->\n         " + encodedMsg+"\n"+"key= "+key);
                break;
            case "decode" :
                key = Integer.parseInt(args[2]);
                String decodedMsg = crypto.decode(message, key);
                System.out.println("decoded message --->\n " + decodedMsg+"\n");
        }
    }
}
