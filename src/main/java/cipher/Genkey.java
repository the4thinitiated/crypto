package cipher;

import java.util.Random;

/**
 * Simple Class with methods to generate encryption keys.
 * @author fionn
 */
public class Genkey {

    private Random random = new Random();

    public int caesarCipher() {
        return random.nextInt(25)+1;

    }
}
